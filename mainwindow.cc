#include "mainwindow.hh"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    initEdits();
    sortEdits();
    initSudoku();
}

void MainWindow::initEdits()
{
    QList<QLineEdit*> lineEdits = findChildren<QLineEdit*>();
    for (QLineEdit* lineEdit : lineEdits) {
        lineEdit->setText("0");
        connect(lineEdit, SIGNAL(returnPressed()), this, SLOT(on_returnPressed_triggered()));
        edits.push_back(lineEdit);
    }
}

void MainWindow::sortEdits()
{
    QLineEdit* ghost;
    bool swap = true;
    for(int j = 0; swap; j++)
    {
        swap = false;
        for(int k = edits.size()-1; k > j; k--)
        {
            if(edits.at(k)->objectName() < edits.at(k-1)->objectName())
            {
                ghost = edits.at(k);
                edits[k] = edits.at(k-1);
                edits[k-1] = ghost;
                swap = true;
            }
        }
    }
//    for (int i = 0, size = edits.size(); i < size; i++) {
//        std::cout << edits.at(i)->objectName().toStdString() << std::endl;
//    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionFill_with_0_triggered()
{
    initSudoku();
    transferSudoku();
    printSudoku();
}

void MainWindow::initSudoku()
{
    sudoku.clear();
    for (int i = 0; i < SUDOKU_NUMBER; i++) {
        Lister row;
        for (int j = 0; j < SUDOKU_NUMBER; j++) {
            row.push_back(std::make_shared<int>(0));
        }
        sudoku.push_back(row);
    }
}

void MainWindow::on_actionFill_with_random_triggered()
{
    initSudoku();
    generateSudoku();
    transferSudoku();
    printSudoku();
}

void MainWindow::generateSudoku()
{
    switch (DIFFICULTY) {
    case DIFF_EASY:
        generateEasy();
        break;
    case DIFF_HARD:
        generateHard();
        break;
    case DIFF_EVIL:
        generateEvil();
        break;
    }
}

void MainWindow::generateEasy()
{
    sudoku[0][2] = std::make_shared<int>(1);
    sudoku[0][4] = std::make_shared<int>(6);
    sudoku[0][5] = std::make_shared<int>(5);
    sudoku[0][7] = std::make_shared<int>(3);
    sudoku[0][8] = std::make_shared<int>(7);

    sudoku[1][0] = std::make_shared<int>(3);
    sudoku[1][4] = std::make_shared<int>(2);
    sudoku[1][5] = std::make_shared<int>(9);
    sudoku[1][6] = std::make_shared<int>(1);
    sudoku[1][7] = std::make_shared<int>(6);

    sudoku[2][1] = std::make_shared<int>(6);
    sudoku[2][2] = std::make_shared<int>(4);

    sudoku[3][1] = std::make_shared<int>(2);
    sudoku[3][2] = std::make_shared<int>(8);
    sudoku[3][5] = std::make_shared<int>(4);
    sudoku[3][8] = std::make_shared<int>(3);

    sudoku[4][3] = std::make_shared<int>(1);
    sudoku[4][5] = std::make_shared<int>(7);

    sudoku[5][0] = std::make_shared<int>(5);
    sudoku[5][3] = std::make_shared<int>(2);
    sudoku[5][6] = std::make_shared<int>(6);
    sudoku[5][7] = std::make_shared<int>(9);

    sudoku[6][0] = std::make_shared<int>(4);
    sudoku[6][6] = std::make_shared<int>(5);
    sudoku[6][7] = std::make_shared<int>(2);

    sudoku[7][1] = std::make_shared<int>(5);
    sudoku[7][2] = std::make_shared<int>(6);
    sudoku[7][3] = std::make_shared<int>(9);
    sudoku[7][4] = std::make_shared<int>(4);
    sudoku[7][8] = std::make_shared<int>(8);

    sudoku[8][0] = std::make_shared<int>(9);
    sudoku[8][1] = std::make_shared<int>(1);
    sudoku[8][3] = std::make_shared<int>(5);
    sudoku[8][4] = std::make_shared<int>(8);
}

void MainWindow::generateHard()
{
    sudoku[0][1] = std::make_shared<int>(9);
    sudoku[0][4] = std::make_shared<int>(3);

    sudoku[1][1] = std::make_shared<int>(4);
    sudoku[1][6] = std::make_shared<int>(8);
    sudoku[1][7] = std::make_shared<int>(5);
    sudoku[1][8] = std::make_shared<int>(6);

    sudoku[2][2] = std::make_shared<int>(5);
    sudoku[2][6] = std::make_shared<int>(3);

    sudoku[3][5] = std::make_shared<int>(7);
    sudoku[3][8] = std::make_shared<int>(1);

    sudoku[4][1] = std::make_shared<int>(2);
    sudoku[4][3] = std::make_shared<int>(1);
    sudoku[4][4] = std::make_shared<int>(6);
    sudoku[4][5] = std::make_shared<int>(9);
    sudoku[4][7] = std::make_shared<int>(3);

    sudoku[5][0] = std::make_shared<int>(5);
    sudoku[5][3] = std::make_shared<int>(8);

    sudoku[6][2] = std::make_shared<int>(3);
    sudoku[6][6] = std::make_shared<int>(4);

    sudoku[7][0] = std::make_shared<int>(8);
    sudoku[7][1] = std::make_shared<int>(6);
    sudoku[7][2] = std::make_shared<int>(1);
    sudoku[7][7] = std::make_shared<int>(7);

    sudoku[8][4] = std::make_shared<int>(8);
    sudoku[8][7] = std::make_shared<int>(2);
}

void MainWindow::generateEvil()
{
    sudoku[0][1] = std::make_shared<int>(4);
    sudoku[0][3] = std::make_shared<int>(9);

    sudoku[1][1] = std::make_shared<int>(8);
    sudoku[1][2] = std::make_shared<int>(6);
    sudoku[1][5] = std::make_shared<int>(1);
    sudoku[1][7] = std::make_shared<int>(2);
    sudoku[1][8] = std::make_shared<int>(7);

    sudoku[2][2] = std::make_shared<int>(7);
    sudoku[2][4] = std::make_shared<int>(6);
    sudoku[2][8] = std::make_shared<int>(4);

    sudoku[3][3] = std::make_shared<int>(7);
    sudoku[3][7] = std::make_shared<int>(3);

    sudoku[4][0] = std::make_shared<int>(6);
    sudoku[4][4] = std::make_shared<int>(5);
    sudoku[4][8] = std::make_shared<int>(8);

    sudoku[5][1] = std::make_shared<int>(7);
    sudoku[5][5] = std::make_shared<int>(9);

    sudoku[6][0] = std::make_shared<int>(3);
    sudoku[6][4] = std::make_shared<int>(1);
    sudoku[6][6] = std::make_shared<int>(2);

    sudoku[7][0] = std::make_shared<int>(8);
    sudoku[7][1] = std::make_shared<int>(5);
    sudoku[7][3] = std::make_shared<int>(6);
    sudoku[7][6] = std::make_shared<int>(9);
    sudoku[7][7] = std::make_shared<int>(1);

    sudoku[8][5] = std::make_shared<int>(5);
    sudoku[8][7] = std::make_shared<int>(8);
}

void MainWindow::transferSudoku()
{
    for (int i = 0; i < SUDOKU_NUMBER; i++) {
        for (int j = 0; j < SUDOKU_NUMBER; j++) {
            edits.at(i * SUDOKU_NUMBER + j)->setText(QString::number(*(sudoku[i][j].get())));
        }
    }
}

void MainWindow::printSudoku()
{
    for (int i = 0; i < SUDOKU_NUMBER; i++) {
        for (int j = 0; j < SUDOKU_NUMBER; j++) {
            std::cout << *(sudoku[i][j].get()) << " ";
        }
        std::cout << std::endl;
    }
}

void MainWindow::on_actionSolve_all_triggered()
{
    auto before = std::chrono::system_clock::now();
    if (backtrack()) {
        transferSudoku();
        printSudoku();
        auto after = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed = after - before;
        std::cout << "Successfully finished in time: " << elapsed.count() << "." << std::endl;
    }
    else std::cout << "Failed to finish." << std::endl;
}

bool MainWindow::backtrack()
{
    QPoint available_cell = findAvailableCell();
    if (!isValid(available_cell)) return true;
    else {
        for (int i = 1; i <= SUDOKU_NUMBER; i++) {
            if (isValid(available_cell, i)) {
                sudoku[available_cell.y()][available_cell.x()] = std::make_shared<int>(i);
                if (backtrack()) return true;
                else sudoku[available_cell.y()][available_cell.x()] = std::make_shared<int>(0);
            }
        }
    }
    return false;
}

bool MainWindow::isValid(QPoint available_cell)
{
    if (available_cell.x() != -1 && available_cell.y() != -1)
        return true;
    else
        return false;
}

bool MainWindow::isValid(QPoint available_cell, int candidate)
{
    int x = available_cell.x(), y = available_cell.y();
    //@HORIZONTAL
    for (int i = 0; i < SUDOKU_NUMBER; i++) {
        if (*(sudoku.at(y).at(i).get()) == candidate) return false;
    }
    //@VERTICAL
    for (int i = 0; i < SUDOKU_NUMBER; i++) {
        if (*(sudoku.at(i).at(x).get()) == candidate) return false;
    }
    //@PROVINCE
    x = std::floor(x / PROVINCE_NUMBER);
    y = std::floor(y / PROVINCE_NUMBER);
    for (int i = 0; i < PROVINCE_NUMBER; i++) {
        for (int j = 0; j < PROVINCE_NUMBER; j++) {
            if (*(sudoku.at(y * PROVINCE_NUMBER + i).at(x * PROVINCE_NUMBER + j).get()) == candidate)
                return false;
        }
    }
    return true;
}

QPoint MainWindow::findAvailableCell()
{
    for (int i = 0; i < SUDOKU_NUMBER; i++) {
        for (int j = 0; j < SUDOKU_NUMBER; j++) {
            if (*(sudoku.at(i).at(j).get()) == 0) {
                return QPoint(j, i);
            }
        }
    }
    return QPoint(-1, -1);
}

void MainWindow::on_returnPressed_triggered()
{
    //@RECEIVE
    QObject* obj = sender();
    QLineEdit* edit = qobject_cast<QLineEdit*>(obj);
    //@DECLARE
    int index = getEditIndex(edit);
    int y = std::floor(index / SUDOKU_NUMBER);
    int x = index - (y * SUDOKU_NUMBER);
    int candidate = edit->text().toInt();
    if (isValid(QPoint(x, y), candidate))
        sudoku[y][x] = std::make_shared<int>(candidate);
    else
        transferSudoku();
    printSudoku();
}

int MainWindow::getEditIndex(QLineEdit* edit)
{
    for (int i = 0, size = edits.size(); i < size; i++) {
        if (edit->objectName().toStdString() == edits.at(i)->objectName().toStdString())
            return i;
    }
    return -1;
}

void MainWindow::on_actionEasy_triggered()
{
    DIFFICULTY = DIFF_EASY;
    ui->actionEasy->setChecked(true);
    ui->actionHard->setChecked(false);
    ui->actionEvil->setChecked(false);
}

void MainWindow::on_actionHard_triggered()
{
    DIFFICULTY = DIFF_HARD;
    ui->actionEasy->setChecked(false);
    ui->actionHard->setChecked(true);
    ui->actionEvil->setChecked(false);
}

void MainWindow::on_actionEvil_triggered()
{
    DIFFICULTY = DIFF_EVIL;
    ui->actionEasy->setChecked(false);
    ui->actionHard->setChecked(false);
    ui->actionEvil->setChecked(true);
}
