#ifndef MAINWINDOW_HH
#define MAINWINDOW_HH

#include <QMainWindow>
#include <QLineEdit>
#include <memory>
#include <chrono>
#include <iostream>

typedef std::shared_ptr<int> Inter;
typedef QList<Inter> Lister;

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    static const int SUDOKU_NUMBER = 9;
    static const int PROVINCE_NUMBER = 3;
    static const int DIFF_EASY = 0;
    static const int DIFF_HARD = 1;
    static const int DIFF_EVIL = 2;
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void on_actionFill_with_0_triggered();
    void on_actionFill_with_random_triggered();
    void on_actionSolve_all_triggered();
    void on_returnPressed_triggered();
    void on_actionEasy_triggered();
    void on_actionHard_triggered();
    void on_actionEvil_triggered();
private:
    Ui::MainWindow *ui;
    QList<Lister> sudoku;
    QVector<QLineEdit*> edits;
    unsigned int DIFFICULTY = DIFF_EASY;
    void initEdits();
    void sortEdits();
    void initSudoku();
    void generateSudoku();
    void generateEasy();
    void generateHard();
    void generateEvil();
    void transferSudoku();
    void printSudoku();
    bool backtrack();
    bool isValid(QPoint);
    bool isValid(QPoint, int);
    QPoint findAvailableCell();
    int getEditIndex(QLineEdit*);
};

#endif // MAINWINDOW_HH
