#-------------------------------------------------
#
# Project created by QtCreator 2014-01-15T11:01:24
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ai_03_su
TEMPLATE = app


SOURCES += main.cc\
        mainwindow.cc

HEADERS  += mainwindow.hh

FORMS    += mainwindow.ui

CONFIG += C++11
